package com.example.desafiomercadolibre.controllers;

import android.text.Editable;

public class ProductController {

    private static ProductController sInstance;
    private String productId;

    public static ProductController getInstance() {
        if (sInstance == null) {
            sInstance = new ProductController();
        }
        return sInstance;
    }

    public void setProductId(String productId) {
        this.productId = productId;
    }

    public String getProductId() {
        return productId;
    }

}
