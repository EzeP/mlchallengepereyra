package com.example.desafiomercadolibre.fragments;

import android.os.Bundle;
import android.provider.ContactsContract;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import com.example.desafiomercadolibre.R;
import com.example.desafiomercadolibre.adapter.ListofItemsSearchAdapter;
import com.example.desafiomercadolibre.controllers.ProductController;
import com.example.desafiomercadolibre.utils.ApiCaller;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import cz.msebera.android.httpclient.Header;

public class DetailItemFragment extends Fragment {

    TextView tv_description;
    TextView tv_title;
    ImageView iv_item;

    private ApiCaller.IApiFacadeListener mListener = new ApiCaller.IApiFacadeListener() {
        @Override
        public void onSuccess(int statusCode, Header[] headers, String res) {
            try {
                JSONObject jsonDetail = new JSONObject(res);
               String description =  jsonDetail.getString("short_description");
               String name = jsonDetail.getString("name");
               JSONObject jsonDesription = new JSONObject(description);
               String image = getPrincipalImage(jsonDetail);
                tv_description.setText(jsonDesription.getString("content"));
                tv_title.setText(name);
                Picasso.get().load(image).into(iv_item);
            } catch (JSONException e) {
                e.printStackTrace();
            }


        }

        @Override
        public void onFailure(int statusCode, Header[] headers, String res, Throwable t) {

            String failure = "";
        }
    };

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        final View root = inflater.inflate(R.layout.fragment_detail_item, container, false);

        tv_description = root.findViewById(R.id.tv_description);
        tv_title = root.findViewById(R.id.tv_title);
        iv_item = root.findViewById(R.id.iv_item);

        ApiCaller.getProductDetail(ProductController.getInstance().getProductId(),getActivity(),mListener);

        return root;



    }

    public String getPrincipalImage(JSONObject objectProducts){
        String urlPicturePrincipal = "";
        try {
            for (int idx = 0; idx < objectProducts.length() ; idx++){
                JSONArray arrayPictures = objectProducts.getJSONArray("pictures");
                urlPicturePrincipal = arrayPictures.getJSONObject(0).getString("url");

            }
        }catch (JSONException e) {
            e.printStackTrace();
        }
        return urlPicturePrincipal;

    }


}
