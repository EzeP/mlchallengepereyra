package com.example.desafiomercadolibre.dialogs;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;

import com.example.desafiomercadolibre.R;


public class LoadingDialog extends Dialog {

    public LoadingDialog(Context context) {
        super(context);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.dialog_loading_dialog);
    }
}
