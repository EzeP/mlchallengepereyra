package com.example.desafiomercadolibre;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ProgressBar;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;


import com.example.desafiomercadolibre.adapter.ListofItemsSearchAdapter;
import com.example.desafiomercadolibre.utils.ApiCaller;
import com.google.android.material.tabs.TabLayout;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import cz.msebera.android.httpclient.Header;

public class MainActivity extends AppCompatActivity {

    ProgressBar progressBar;
    int firstVisibleItem, visibleItemCount, totalItemCount, lastVisibleItem, limit;
    int offset = 0;
    GridLayoutManager mLayoutManager;
    private int previousTotal = 0;
    final List<JSONObject> list = new ArrayList<JSONObject>();


    private ApiCaller.IApiFacadeListener mListener = new ApiCaller.IApiFacadeListener() {
        @Override
        public void onSuccess(int statusCode, Header[] headers, String res) {
            if (res.length() != 0){
                progressBar.setVisibility(View.GONE);
                limit = 0;
                try {
                    JSONObject items = new JSONObject();
                    JSONObject objectProducts = new JSONObject(res);
                    JSONArray arrayProducts = objectProducts.getJSONArray("results");
                    System.out.println(arrayProducts);

                    while (limit < 11){
                        for (int idx = 0; idx < arrayProducts.length(); idx++) {
                            list.add(arrayProducts.getJSONObject(idx));
                            limit++;
                        }
                    }

                    myRecyclerItems = new ListofItemsSearchAdapter(list, getApplicationContext());
                    mRvActivitiesSupported.setAdapter(myRecyclerItems);
                    myRecyclerItems.notifyDataSetChanged();

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }else{
                progressBar.setVisibility(View.GONE);

            }



        }

        @Override
        public void onFailure(int statusCode, Header[] headers, String res, Throwable t) {

            String failure = "";
        }
    };

    private RecyclerView mRvActivitiesSupported;
    private ListofItemsSearchAdapter myRecyclerItems;
    private ImageButton btn_search;
    private EditText et_search;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.searchpage);

        btn_search = findViewById(R.id.btn_search);
        et_search = findViewById(R.id.et_search);
        progressBar = findViewById(R.id.progress_bar);
        limit = 0;

        mLayoutManager = new GridLayoutManager(getApplicationContext(),1);

        progressBar.setVisibility(View.GONE);

        btn_search.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ApiCaller.SearchQuery(et_search.getText().toString(), 0 ,MainActivity.this, mListener);
            }
        });

        mRvActivitiesSupported = findViewById(R.id.rv_items);
        mRvActivitiesSupported.setLayoutManager(mLayoutManager);
        mRvActivitiesSupported.setHasFixedSize(true);

        mRvActivitiesSupported.addOnScrollListener(new RecyclerView.OnScrollListener() {

            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);

                visibleItemCount = mRvActivitiesSupported.getChildCount();
                totalItemCount = mLayoutManager.getItemCount();
                firstVisibleItem = mLayoutManager.findFirstVisibleItemPosition();
                lastVisibleItem = mLayoutManager.findLastVisibleItemPosition();


                if (!recyclerView.canScrollVertically(1)) {
                    offset+=10;
                    progressBar.setVisibility(View.VISIBLE);
                    ApiCaller.SearchQuery(et_search.getText().toString(),offset,MainActivity.this,mListener);

                }


            }

        });
    }

}
