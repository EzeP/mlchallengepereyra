package com.example.desafiomercadolibre.utils;

import android.content.Context;

import androidx.fragment.app.FragmentActivity;

import com.example.desafiomercadolibre.MainActivity;
import com.example.desafiomercadolibre.dialogs.LoadingDialog;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.TextHttpResponseHandler;


import org.json.JSONException;
import org.json.JSONObject;
import java.util.Locale;

import cz.msebera.android.httpclient.Header;
import cz.msebera.android.httpclient.entity.StringEntity;


public class ApiCaller {

    public static final int FROM_CACHE = 304;
    public static final int PAGINATION = 10;
    private static final String TAG = ApiCaller.class.getSimpleName();


    public static void SearchQuery(String query, int offset ,MainActivity mainActivity, IApiFacadeListener listener) {
        try {
            LoadingDialog loadingDialog = new LoadingDialog(mainActivity);
            genericCallGet("https://api.mercadolibre.com/products/search?status=active&site_id=MLA&q=" + query + "&offset=" + offset , mainActivity, listener, false, loadingDialog);
            System.out.println("https://api.mercadolibre.com/products/search?status=active&site_id=MLA&q=" + query + "&offset=" + offset);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public static void getProductDetail(String id , FragmentActivity mainActivity, IApiFacadeListener listener) {
        try {
            LoadingDialog loadingDialog = new LoadingDialog(mainActivity);
            genericCallGet("https://api.mercadolibre.com/products/" + id , mainActivity, listener, false, loadingDialog);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }


    private static void genericCallGet(final String apiName, final Context context,
                                       final IApiFacadeListener listener, final boolean requireUserLoggedIn,
                                       LoadingDialog loadingDialog)
            throws JSONException {


        if (loadingDialog == null && context != null) {
            if (context instanceof android.app.Activity) {
                if (!((android.app.Activity) context).isFinishing()) {
                    loadingDialog = new LoadingDialog(context);
                    loadingDialog.show();
                }
            } else {
                try {
                    loadingDialog = new LoadingDialog(context);
                    loadingDialog.show();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }

        final AsyncHttpClient client = new AsyncHttpClient(true, 80, 443);


        //loadGenericArgs(args, createDataOfDevice(context));

        StringEntity entity = null;
        try {


        } catch (Exception ex) {

        }

        final LoadingDialog finalLoadingDialog = loadingDialog;
        final StringEntity finalEntity = entity;
        final LoadingDialog finalLoadingDialog1 = loadingDialog;
        String url = apiName;
        client.get(context
                , url
                , new TextHttpResponseHandler() {

                    @Override
                    public void onSuccess(int statusCode, Header[] headers, String originalRes) {
                        if (finalLoadingDialog != null && finalLoadingDialog.isShowing()) {
                            try {
                                finalLoadingDialog.dismiss();
                            } catch (IllegalArgumentException e) {
                                e.printStackTrace();
                            }
                        }
                        if (listener != null) {
                            //Crashlytics.log(Log.DEBUG, TAG, String.format("%s - onSuccess()", apiName));
                            String res = originalRes;
                            try {
                                //JSONObject response = new JSONObject(originalRes);
                                //response.put("serviceName", apiName);
                                //res = response.toString();
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                            listener.onSuccess(statusCode, headers, res);
                        } else {
                            //Crashlytics.log(Log.DEBUG, TAG, String.format("%s: cache equals to new info", apiName));
                        }

                    }

                    @Override
                    public void onFailure(int statusCode, Header[] headers, String res, Throwable t) {
                        try {
                            JSONObject argst = new JSONObject();

                            if (finalLoadingDialog != null && finalLoadingDialog.isShowing()) {
                                try {
                                    finalLoadingDialog.dismiss();
                                } catch (IllegalArgumentException e) {
                                    e.printStackTrace();
                                }
                            }

                        } catch (IllegalArgumentException e) {
                            e.printStackTrace();
                        }
                        if (listener != null) {
                            //Crashlytics.log(Log.WARN, TAG, String.format("%s - onFailure()", apiName));
                            listener.onFailure(statusCode, headers, res, t);
                        }
                    }
                });
    }



    public interface IApiFacadeListener {
        void onSuccess(int statusCode, Header[] headers, String res);
        void onFailure(int statusCode, Header[] headers, String res, Throwable t);
    }
}
