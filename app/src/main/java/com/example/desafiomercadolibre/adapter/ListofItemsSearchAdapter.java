package com.example.desafiomercadolibre.adapter;

import android.content.Context;
import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.RecyclerView;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.CountDownTimer;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.example.desafiomercadolibre.ProductActivity;
import com.example.desafiomercadolibre.R;
import com.example.desafiomercadolibre.controllers.ProductController;
import com.example.desafiomercadolibre.fragments.DetailItemFragment;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

public class ListofItemsSearchAdapter extends RecyclerView.Adapter<ListofItemsSearchAdapter.KeyHolder> {

    String id;

    private List<JSONObject> mListActivitiesSupported;
    private Context mContext;


    public ListofItemsSearchAdapter(List<JSONObject> listData, Context context) {
        this.mListActivitiesSupported = listData;
        this.mContext = context;
    }


    public class KeyHolder extends RecyclerView.ViewHolder {

        ConstraintLayout llContentAll;
        ImageView ivItem;
        TextView tvTitle;
        TextView tvPrice;


        private KeyHolder(View itemView) {
            super(itemView);

           // llContentAll = itemView.findViewById(R.id.clContentall);
            ivItem = itemView.findViewById(R.id.iv_item);
            tvTitle = itemView.findViewById(R.id.tv_title);
            tvPrice = itemView.findViewById(R.id.tv_price);
        }
    }

    @NonNull
    @Override
    public ListofItemsSearchAdapter.KeyHolder onCreateViewHolder(@NonNull final ViewGroup parent, final int viewType) {
        JSONObject objectActivity = mListActivitiesSupported.get(viewType);
        mContext = parent.getContext();
        View view = null;
        view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.modal_each_item_in_search, parent, false);

        return new ListofItemsSearchAdapter.KeyHolder(view);

    }



    @Override
    public void onBindViewHolder(@NonNull final ListofItemsSearchAdapter.KeyHolder holder, final int position) {
        final JSONObject objectActivity = mListActivitiesSupported.get(position);

        try{
            String title = objectActivity.getString("name");
            String price = "$ 300";
            String urlImage = getPrincipalImage(objectActivity);
            id = objectActivity.getString("id");
            holder.tvTitle.setText(title);
            holder.tvPrice.setText(price);
            Picasso.get().load(urlImage).into(holder.ivItem);

        }catch (JSONException e){
            e.printStackTrace();
        }


        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ProductController.getInstance().setProductId(id);
                Intent intent = new Intent(mContext, ProductActivity.class);
                mContext.startActivity(intent);
            }
        });



    }

    public String getPrincipalImage(JSONObject objectProducts){
        String urlPicturePrincipal = "";
        try {
        for (int idx = 0; idx < objectProducts.length() ; idx++){
                JSONArray arrayPictures = objectProducts.getJSONArray("pictures");
                urlPicturePrincipal = arrayPictures.getJSONObject(0).getString("url");

        }
        }catch (JSONException e) {
            e.printStackTrace();
        }
        return urlPicturePrincipal;

    }


    @Override
    public int getItemCount() {
        return mListActivitiesSupported.size();
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }
}
